import 'package:flutter/material.dart';
import 'package:lingumoro/global/utils/utils.dart';
import 'package:lingumoro/global/widgets/action_button.dart';

import 'class_tile.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    setStatusBarColor(Colors.white);
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                // border: Border(bottom: BorderSide()),
                boxShadow: kElevationToShadow[4],
                color: Colors.white,
              ),
              child: Row(
                children: [
                  ActionButton(
                    color: Colors.white,
                    boxShadows: kElevationToShadow[6]!,
                    padding: const EdgeInsetsDirectional.all(6),
                    child: SizedBox(
                      width: 20,
                      height: 20,
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          PositionedDirectional(
                            start: 2,
                            child: Image.asset(
                              'assets/icons/drawer.png',
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ),
                    onTap: () {},
                  ),
                  const SizedBox(width: 12),
                  const Expanded(
                    child: Text(
                      'UPCOMING LESSONS',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  const SizedBox(width: 12),
                  ActionButton(
                    color: Colors.white,
                    boxShadows: kElevationToShadow[6]!,
                    padding: const EdgeInsetsDirectional.all(6),
                    child: SizedBox(
                      width: 20,
                      height: 20,
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          PositionedDirectional(
                            start: 1,
                            child: Image.asset(
                              'assets/icons/search.png',
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ),
                    onTap: () {},
                  ),
                  SizedBox(width: 12),
                  ActionButton(
                    color: Colors.white,
                    boxShadows: kElevationToShadow[6]!,
                    padding: const EdgeInsetsDirectional.all(6),
                    child: SizedBox(
                      width: 20,
                      height: 20,
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          PositionedDirectional(
                            start: 3,
                            child: Image.asset(
                              'assets/icons/bell.png',
                              color: Colors.black,
                              width: 15,
                            ),
                          ),
                        ],
                      ),
                    ),
                    onTap: () {},
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView.separated(
                itemCount: 4,
                padding: const EdgeInsets.all(16),
                itemBuilder: (context, index) {
                  return ClassTile();
                },
                separatorBuilder: (context, index) {
                  return SizedBox(height: 16);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class LessonModel {
  LessonModel({
    required this.languageIcon,
    required this.name,
  });

  String languageIcon;
  String name;
}
