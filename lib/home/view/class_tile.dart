import 'package:flutter/material.dart';
import 'package:lingumoro/global/widgets/action_button.dart';

class ClassTile extends StatelessWidget {
  const ClassTile({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 20,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: kElevationToShadow[6],
        borderRadius: BorderRadius.circular(35),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsetsDirectional.only(
              start: 24.0,
              end: 14,
            ),
            child: Row(
              children: [
                Container(
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    boxShadow: kElevationToShadow[4],
                  ),
                  child: Image.asset('assets/icons/english.png'),
                ),
                SizedBox(width: 12),
                Expanded(
                  child: Text(
                    'English class',
                    style: Theme.of(context)
                        .textTheme
                        .headline6!
                        .copyWith(fontSize: 16),
                  ),
                ),
                SizedBox(width: 12),
                PopupMenuButton(
                  enabled: false,
                  itemBuilder: (context) => [],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 16),
                Container(
                  padding: const EdgeInsets.only(bottom: 20),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 3,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  child: Row(
                    children: [
                      Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                          border: Border.all(),
                          shape: BoxShape.circle,
                          boxShadow: kElevationToShadow[4],
                        ),
                        child: Image.asset('assets/icons/person.png'),
                      ),
                      const SizedBox(width: 16),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              'Student name here',
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            const SizedBox(height: 6),
                            Text(
                              'Pre-intermediate',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText2!
                                  .copyWith(color: Colors.grey),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(width: 16),
                      ActionButton(
                        color: Colors.white,
                        boxShadows: kElevationToShadow[6]!,
                        padding: EdgeInsetsDirectional.all(7),
                        child: Image.asset(
                          'assets/icons/chat.png',
                          width: 18,
                        ),
                        onTap: () {},
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 16),
                SizedBox(
                  height: 56,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          width: 30,
                          height: 30,
                          child: Image.asset('assets/icons/calendar.png'),
                        ),
                      ),
                      const SizedBox(width: 20),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            const SizedBox(height: 7),
                            Text('Wed,16 November 2022'),
                            const SizedBox(height: 10),
                            RichText(
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              text: TextSpan(
                                text: '9:00 am - 9:45 am',
                                style: Theme.of(context).textTheme.bodyText2,
                                children: [
                                  TextSpan(text: '  '),
                                  TextSpan(
                                    text: 'Ukraine time',
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 16),
                Row(
                  children: [
                    Container(
                      width: 30,
                      height: 30,
                      child: Image.asset('assets/icons/clock.png'),
                    ),
                    const SizedBox(width: 20),
                    RichText(
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      text: TextSpan(
                        text: 'Class duration',
                        style: Theme.of(context).textTheme.bodyText1,
                        children: [
                          TextSpan(text: '  '),
                          TextSpan(
                            text: '45min',
                            style: Theme.of(context).textTheme.bodyText2,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 25),
                Row(
                  children: [
                    Expanded(
                      flex: 4,
                      child: ActionButton(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 12,
                        ),
                        boxShadows: kElevationToShadow[6]!,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 13,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/icons/link.png',
                                width: 18,
                              ),
                              const SizedBox(width: 3),
                              Expanded(
                                child: const Text(
                                  'JOIN WITH LINK',
                                  textAlign: TextAlign.center,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 13,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        gradient: LinearGradient(
                          begin: AlignmentDirectional.centerStart,
                          end: AlignmentDirectional.centerEnd,
                          colors: [
                            Colors.blue,
                            Colors.blue.shade800,
                          ],
                        ),
                        onTap: () {},
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                      flex: 3,
                      child: ActionButton(
                        padding: const EdgeInsets.all(14),
                        boxShadows: kElevationToShadow[6]!,
                        title: 'POSTPONED',
                        textStyle: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 13,
                        ),
                        gradient: LinearGradient(
                          begin: AlignmentDirectional.centerStart,
                          end: AlignmentDirectional.centerEnd,
                          colors: [
                            Colors.red.shade800,
                            Colors.red,
                          ],
                        ),
                        onTap: () {},
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
