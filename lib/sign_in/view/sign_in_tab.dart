import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:lingumoro/global/widgets/action_button.dart';
import 'package:lingumoro/global/widgets/my_text_field.dart';
import 'package:lingumoro/global/routes/routes.gr.dart';

class SignInTab extends StatelessWidget {
  const SignInTab({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 35),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MyTextField(
            isDense: true,
            boxShadow: kElevationToShadow[6],
            borderRadius: BorderRadius.circular(30),
            fill: true,
            fillColor: Colors.white,
            hintText: 'Email or phone no.',
            hintStyle: const TextStyle(
              color: Colors.grey,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 2,
                color: Colors.blue,
              ),
            ),
          ),
          const SizedBox(height: 20),
          MyTextField(
            isDense: true,
            boxShadow: kElevationToShadow[6],
            borderRadius: BorderRadius.circular(30),
            fill: true,
            fillColor: Colors.white,
            hintText: 'Password',
            isPassword: true,
            hintStyle: const TextStyle(
              color: Colors.grey,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 2,
                color: Colors.blue,
              ),
            ),
          ),
          const SizedBox(height: 40),
          ActionButton(
            padding: const EdgeInsets.all(14),
            boxShadows: kElevationToShadow[6]!,
            title: 'SIGN IN',
            gradient: LinearGradient(
              begin: AlignmentDirectional.topEnd,
              end: AlignmentDirectional.bottomEnd,
              colors: [
                Colors.blue.shade700,
                Colors.blue,
              ],
            ),
            onTap: () {
              debugPrint('Goooooo');
              context.router.replaceAll(
                [const MainNavigationRoute()],
              );
            },
          ),
          const SizedBox(height: 20),
          ActionButton(
            padding: const EdgeInsets.all(14),
            boxShadows: kElevationToShadow[6]!,
            child: const Text(
              'FORGET PASSWORD',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
            color: Colors.white,
            border: Border.all(
              width: 2,
              color: Colors.blue,
            ),
            onTap: () {},
          ),
          const SizedBox(height: 50),
          Column(
            children: [
              Text('or sign in with'),
              const SizedBox(height: 12),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ActionButton(
                    child: Image.asset('assets/icons/google.png'),
                    padding: const EdgeInsets.symmetric(
                      vertical: 12,
                      horizontal: 12,
                    ),
                    color: Colors.black,
                    borderRadius: 40,
                    onTap: () {},
                  ),
                  const SizedBox(width: 10),
                  ActionButton(
                    child: Image.asset('assets/icons/facebook.png'),
                    padding: const EdgeInsets.symmetric(
                      vertical: 12,
                      horizontal: 18,
                    ),
                    color: Colors.black,
                    borderRadius: 40,
                    onTap: () {},
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
