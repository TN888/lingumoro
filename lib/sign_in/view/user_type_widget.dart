import 'package:flutter/material.dart';

class UserTypeWidget extends StatefulWidget {
  const UserTypeWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<UserTypeWidget> createState() => _UserTypeWidgetState();
}

class _UserTypeWidgetState extends State<UserTypeWidget> {
  int selectedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              boxShadow: kElevationToShadow[6],
              color: Colors.white,
              border: Border.all(
                color: selectedIndex == 0 ? Colors.blue : Colors.white,
                width: 3,
              ),
            ),
            child: Material(
              type: MaterialType.transparency,
              borderRadius: BorderRadius.circular(25),
              child: InkWell(
                borderRadius: BorderRadius.circular(25),
                child: const Padding(
                  padding: EdgeInsets.all(16),
                  child: Text('Student'),
                ),
                onTap: () {
                  setState(() {
                    selectedIndex = 0;
                  });
                },
              ),
            ),
          ),
        ),
        const SizedBox(width: 12),
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              boxShadow: kElevationToShadow[6],
              color: Colors.white,
              border: Border.all(
                color: selectedIndex == 1 ? Colors.blue : Colors.white,
                width: 3,
              ),
            ),
            child: Material(
              type: MaterialType.transparency,
              borderRadius: BorderRadius.circular(25),
              child: InkWell(
                borderRadius: BorderRadius.circular(25),
                child: const Padding(
                  padding: EdgeInsets.all(16),
                  child: Text('Teacher'),
                ),
                onTap: () {
                  debugPrint('IX coders');
                  setState(() {
                    selectedIndex = 1;
                  });
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
