import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:lingumoro/global/decorations/borders.dart';
import 'package:lingumoro/global/routes/routes.gr.dart';
import 'package:lingumoro/global/utils/utils.dart';
import 'package:lingumoro/global/widgets/action_button.dart';
import 'package:lingumoro/global/widgets/my_text_field.dart';

import 'sign_in_tab.dart';
import 'sign_up_tab.dart';

class SignInView extends StatefulWidget {
  const SignInView({Key? key}) : super(key: key);

  @override
  _SignInViewState createState() => _SignInViewState();
}

class _SignInViewState extends State<SignInView> {
  @override
  void initState() {
    super.initState();

    buildBorders();
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarColor(Colors.transparent);
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: SingleChildScrollView(
            child: SizedBox(
              height: MediaQuery.of(context).size.height,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding:
                        const EdgeInsetsDirectional.only(start: 10.0, end: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ActionButton(
                          color: Colors.white,
                          boxShadows: kElevationToShadow[6]!,
                          padding: const EdgeInsetsDirectional.all(6),
                          child: SizedBox(
                            width: 20,
                            height: 20,
                            child: Stack(
                              alignment: Alignment.center,
                              children: const [
                                PositionedDirectional(
                                  start: 4,
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    size: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {},
                        ),
                        TextButton(
                          onPressed: () {},
                          child: Text('Skip'),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: const [
                        SizedBox(height: 35),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 35),
                          child: TabBar(
                            labelColor: Colors.black,
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                            unselectedLabelColor: Colors.grey,
                            indicatorSize: TabBarIndicatorSize.label,
                            indicatorColor: Colors.red,
                            indicatorWeight: 3,
                            tabs: [
                              Tab(
                                text: 'SIGN IN',
                              ),
                              Tab(
                                text: 'SIGN UP',
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: TabBarView(
                            children: [
                              SignInTab(),
                              SignUpTab(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
