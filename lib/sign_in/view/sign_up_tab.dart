import 'package:flutter/material.dart';
import 'package:lingumoro/global/widgets/action_button.dart';
import 'package:lingumoro/global/widgets/my_text_field.dart';

import 'user_type_widget.dart';

class SignUpTab extends StatelessWidget {
  const SignUpTab({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 35),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Spacer(flex: 2),
          MyTextField(
            isDense: true,
            boxShadow: kElevationToShadow[6],
            borderRadius: BorderRadius.circular(30),
            fill: true,
            fillColor: Colors.white,
            hintText: 'Name',
            hintStyle: const TextStyle(
              color: Colors.grey,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 2,
                color: Colors.blue,
              ),
            ),
          ),
          const SizedBox(height: 16),
          MyTextField(
            isDense: true,
            boxShadow: kElevationToShadow[6],
            borderRadius: BorderRadius.circular(30),
            fill: true,
            fillColor: Colors.white,
            hintText: 'Email or phone no.',
            hintStyle: const TextStyle(
              color: Colors.grey,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 2,
                color: Colors.blue,
              ),
            ),
          ),
          const SizedBox(height: 16),
          const UserTypeWidget(),
          const SizedBox(height: 16),
          MyTextField(
            isDense: true,
            boxShadow: kElevationToShadow[6],
            borderRadius: BorderRadius.circular(30),
            fill: true,
            fillColor: Colors.white,
            hintText: 'Password',
            hintStyle: const TextStyle(
              color: Colors.grey,
            ),
            isPassword: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 2,
                color: Colors.blue,
              ),
            ),
          ),
          const SizedBox(height: 16),
          MyTextField(
            isDense: true,
            boxShadow: kElevationToShadow[6],
            borderRadius: BorderRadius.circular(30),
            fill: true,
            fillColor: Colors.white,
            hintText: 'Confirm password',
            hintStyle: const TextStyle(
              color: Colors.grey,
            ),
            isPassword: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 0,
                color: Colors.white,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: const BorderSide(
                width: 2,
                color: Colors.blue,
              ),
            ),
          ),
          const SizedBox(height: 20),
          ActionButton(
            padding: const EdgeInsets.all(14),
            boxShadows: kElevationToShadow[6]!,
            title: 'CREATE ACCOUNT',
            gradient: LinearGradient(
              begin: AlignmentDirectional.topEnd,
              end: AlignmentDirectional.bottomEnd,
              colors: [
                Colors.blue.shade700,
                Colors.blue,
              ],
            ),
            onTap: () {},
          ),
          Spacer(flex: 4),
        ],
      ),
    );
  }
}
