import 'package:flutter/material.dart';

class LanguageTile extends StatelessWidget {
  const LanguageTile({
    Key? key,
    this.isSelected = false,
    required this.onTap,
    required this.child,
  }) : super(key: key);

  final bool isSelected;
  final VoidCallback onTap;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        boxShadow: kElevationToShadow[6],
        color: Colors.white,
      ),
      alignment: Alignment.center,
      child: Material(
        borderRadius: BorderRadius.circular(20),
        type: MaterialType.transparency,
        child: InkWell(
          borderRadius: BorderRadius.circular(20),
          onTap: () {
            onTap();
          },
          child: Stack(
            children: [
              if (isSelected)
                PositionedDirectional(
                  start: 12,
                  top: 12,
                  width: 25,
                  height: 25,
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 25,
                        height: 25,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Colors.blue,
                              Colors.blue.shade800,
                            ],
                          ),
                        ),
                        alignment: Alignment.center,
                      ),
                      const PositionedDirectional(
                        bottom: 1,
                        child: Icon(
                          Icons.check_rounded,
                          color: Colors.white,
                          size: 20,
                        ),
                      ),
                    ],
                  ),
                ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: child,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
