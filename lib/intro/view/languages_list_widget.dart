import 'package:flutter/material.dart';
import 'package:lingumoro/intro/view/language_tile.dart';

class LanguagesListWidget extends StatefulWidget {
  const LanguagesListWidget({
    Key? key,
    required this.onSelected,
    required this.languages,
  }) : super(key: key);

  final ValueChanged<String> onSelected;
  final List<String> languages;

  @override
  _LanguagesListWidgetState createState() => _LanguagesListWidgetState();
}

class _LanguagesListWidgetState extends State<LanguagesListWidget> {
  int selectedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text(
          'Choose a language',
          style: TextStyle(color: Colors.grey),
        ),
        const SizedBox(height: 35),
        ListView.separated(
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return LanguageTile(
              isSelected: index == selectedIndex,
              onTap: () {
                setState(() {
                  selectedIndex = index;
                });
              },
              child: Text(
                widget.languages[index],
                style: TextStyle(
                  color: index == selectedIndex
                      ? Colors.blue.shade700
                      : Colors.black,
                  fontWeight: index == selectedIndex
                      ? FontWeight.bold
                      : FontWeight.normal,
                ),
              ),
            );
          },
          separatorBuilder: (context, index) {
            return const SizedBox(
              height: 16,
            );
          },
          itemCount: widget.languages.length,
        ),
      ],
    );
  }
}
