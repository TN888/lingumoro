import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:lingumoro/global/routes/routes.gr.dart';
import 'package:lingumoro/global/utils/utils.dart';
import 'package:lingumoro/global/widgets/action_button.dart';
import 'package:lingumoro/intro/view/languages_list_widget.dart';

class ChooseLanguageView extends StatefulWidget {
  const ChooseLanguageView({Key? key}) : super(key: key);

  @override
  _ChooseLanguageViewState createState() => _ChooseLanguageViewState();
}

class _ChooseLanguageViewState extends State<ChooseLanguageView> {
  @override
  Widget build(BuildContext context) {
    setStatusBarColor(Colors.transparent);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          Padding(
            padding: const EdgeInsetsDirectional.only(end: 10.0),
            child: Row(
              children: [
                TextButton(
                  onPressed: () {},
                  child: Text('Skip'),
                ),
              ],
            ),
          )
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Column(
                children: [
                  SizedBox(
                    width: 100,
                    height: 100,
                    child: Image.asset('assets/icons/logo.png'),
                  ),
                  const SizedBox(height: 16),
                  RichText(
                    text: const TextSpan(
                      text: 'Welcome to Lingu',
                      style: TextStyle(color: Colors.black, fontSize: 18),
                      children: [
                        TextSpan(
                          text: 'Moro',
                          style: TextStyle(
                            color: Colors.red,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  LanguagesListWidget(
                    onSelected: (value) {},
                    languages: ['Ukranian', 'Russian'],
                  ),
                  const SizedBox(height: 40),
                  ActionButton(
                    padding: const EdgeInsets.all(14),
                    boxShadows: kElevationToShadow[6]!,
                    title: 'Continue',
                    gradient: LinearGradient(
                      begin: AlignmentDirectional.topEnd,
                      end: AlignmentDirectional.bottomEnd,
                      colors: [
                        Colors.blue.shade700,
                        Colors.blue,
                      ],
                    ),
                    onTap: () {
                      context.router.push(const SignInRoute());
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
