import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';

class IntroView extends StatefulWidget {
  const IntroView({Key? key}) : super(key: key);

  @override
  _IntroViewState createState() => _IntroViewState();
}

class _IntroViewState extends State<IntroView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IntroductionScreen(
        dotsContainerDecorator: BoxDecoration(
          color: Colors.blue,
        ),
        pages: [
          PageViewModel(
            title: 'title',
            bodyWidget: Container(
              width: MediaQuery.of(context).size.width,
              height: 100,
              color: Colors.blue,
            ),
            image: Container(
              color: Colors.pink,
            ),
            decoration: PageDecoration(
              fullScreen: true,
            ),
          ),
        ],
        controlsPadding: EdgeInsets.only(bottom: 16),
        onSkip: () {
          // You can also override onSkip callback
        },
        showDoneButton: false,
        showNextButton: false,
        showBackButton: false,
        showSkipButton: false,
        skip: const Icon(Icons.arrow_forward_ios_rounded),
        isProgress: true,
        dotsDecorator: DotsDecorator(
          size: const Size.square(10.0),
          activeSize: const Size(20.0, 10.0),
          activeColor: Colors.white,
          // colors: [
          //   Colors.grey,
          //   Colors.grey,
          //   Colors.grey,
          // ],

          color: Colors.black26,
          spacing: const EdgeInsets.symmetric(horizontal: 3.0),

          activeShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
          ),
        ),
      ),
    );
  }
}
