import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';

import 'package:auto_route/auto_route.dart';
import 'package:lingumoro/global/routes/routes.gr.dart';
import 'package:lingumoro/global/utils/utils.dart';

class MainNavigationView extends StatefulWidget {
  const MainNavigationView({Key? key}) : super(key: key);

  @override
  _MainNavigationViewState createState() => _MainNavigationViewState();
}

class _MainNavigationViewState extends State<MainNavigationView> {
  @override
  Widget build(BuildContext context) {
    setStatusBarColor(Colors.transparent);
    return AutoTabsScaffold(
      routes: const [
        HomeRoute(),
        LessonsInCalendarRoute(),
      ],
      bottomNavigationBuilder: (_, tabsRouter) {
        return ConvexAppBar(
          backgroundColor: Colors.white,
          activeColor: Colors.red,
          curveSize: 80,
          style: TabStyle.reactCircle,
          // top: -40,
          items: [
            TabItem(
              icon: Image.asset('assets/icons/home.png'),
              activeIcon: Center(
                child: Image.asset(
                  'assets/icons/home_selected.png',
                  width: 30,
                ),
              ),
            ),
            TabItem(
              icon: Image.asset('assets/icons/lessons.png'),
              activeIcon: Center(
                child: Image.asset(
                  'assets/icons/lessons_selected.png',
                  width: 30,
                ),
              ),
            ),
            TabItem(
              icon: Image.asset('assets/icons/messages.png'),
              activeIcon: Center(
                child: Image.asset(
                  'assets/icons/messages.png',
                  color: Colors.white,
                  width: 30,
                ),
              ),
            ),
            TabItem(
              icon: Image.asset('assets/icons/bookmark.png'),
              activeIcon: Center(
                child: Image.asset(
                  'assets/icons/bookmark.png',
                  color: Colors.white,
                  width: 30,
                ),
              ),
            ),
            TabItem(
              icon: Image.asset('assets/icons/profile.png'),
              activeIcon: Center(
                child: Image.asset(
                  'assets/icons/profile.png',
                  color: Colors.white,
                  width: 30,
                ),
              ),
            ),
          ],
          initialActiveIndex: tabsRouter.activeIndex,
          onTap: tabsRouter.setActiveIndex,
        );
      },
    );
  }
}
