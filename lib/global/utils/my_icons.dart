// ignore_for_file: constant_identifier_names

import 'package:flutter/widgets.dart';

class MyIcons {
  MyIcons._();

  static const _kFontFam = 'MyIcons';

  static const IconData payment = IconData(0xe800, fontFamily: _kFontFam);
  static const IconData eye_slash = IconData(0xf070, fontFamily: _kFontFam);
  static const IconData ticket = IconData(0xf145, fontFamily: _kFontFam);
  static const IconData paypal = IconData(0xf342, fontFamily: _kFontFam);
}
