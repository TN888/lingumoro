import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:lingumoro/global/main_navigation/view/main_navigation_view.dart';
import 'package:lingumoro/home/view/home.view.dart';
import 'package:lingumoro/intro/view/choose_language_view.dart';
import 'package:lingumoro/lessons_in_calendar/view/lessons_in_calendar_view.dart';
import 'package:lingumoro/sign_in/view/sign_in_view.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'View,Route',
  routes: <AutoRoute>[
    AutoRoute(
      page: ChooseLanguageView,
      initial: true,
    ),
    AutoRoute(
      page: SignInView,
    ),
    AutoRoute(
      page: MainNavigationView,
      children: [
        AutoRoute(page: HomeView),
        AutoRoute(page: LessonsInCalendarView),
      ],
    ),
  ],
)
class $AppRouter {}
