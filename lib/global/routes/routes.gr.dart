// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i6;
import 'package:flutter/material.dart' as _i7;

import '../../home/view/home.view.dart' as _i4;
import '../../intro/view/choose_language_view.dart' as _i1;
import '../../lessons_in_calendar/view/lessons_in_calendar_view.dart' as _i5;
import '../../sign_in/view/sign_in_view.dart' as _i2;
import '../main_navigation/view/main_navigation_view.dart' as _i3;

class AppRouter extends _i6.RootStackRouter {
  AppRouter([_i7.GlobalKey<_i7.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i6.PageFactory> pagesMap = {
    ChooseLanguageRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i1.ChooseLanguageView());
    },
    SignInRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i2.SignInView());
    },
    MainNavigationRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i3.MainNavigationView());
    },
    HomeRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i4.HomeView());
    },
    LessonsInCalendarRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i5.LessonsInCalendarView());
    }
  };

  @override
  List<_i6.RouteConfig> get routes => [
        _i6.RouteConfig(ChooseLanguageRoute.name, path: '/'),
        _i6.RouteConfig(SignInRoute.name, path: '/sign-in-view'),
        _i6.RouteConfig(MainNavigationRoute.name,
            path: '/main-navigation-view',
            children: [
              _i6.RouteConfig(HomeRoute.name,
                  path: 'home-view', parent: MainNavigationRoute.name),
              _i6.RouteConfig(LessonsInCalendarRoute.name,
                  path: 'lessons-in-calendar-view',
                  parent: MainNavigationRoute.name)
            ])
      ];
}

/// generated route for
/// [_i1.ChooseLanguageView]
class ChooseLanguageRoute extends _i6.PageRouteInfo<void> {
  const ChooseLanguageRoute() : super(ChooseLanguageRoute.name, path: '/');

  static const String name = 'ChooseLanguageRoute';
}

/// generated route for
/// [_i2.SignInView]
class SignInRoute extends _i6.PageRouteInfo<void> {
  const SignInRoute() : super(SignInRoute.name, path: '/sign-in-view');

  static const String name = 'SignInRoute';
}

/// generated route for
/// [_i3.MainNavigationView]
class MainNavigationRoute extends _i6.PageRouteInfo<void> {
  const MainNavigationRoute({List<_i6.PageRouteInfo>? children})
      : super(MainNavigationRoute.name,
            path: '/main-navigation-view', initialChildren: children);

  static const String name = 'MainNavigationRoute';
}

/// generated route for
/// [_i4.HomeView]
class HomeRoute extends _i6.PageRouteInfo<void> {
  const HomeRoute() : super(HomeRoute.name, path: 'home-view');

  static const String name = 'HomeRoute';
}

/// generated route for
/// [_i5.LessonsInCalendarView]
class LessonsInCalendarRoute extends _i6.PageRouteInfo<void> {
  const LessonsInCalendarRoute()
      : super(LessonsInCalendarRoute.name, path: 'lessons-in-calendar-view');

  static const String name = 'LessonsInCalendarRoute';
}
