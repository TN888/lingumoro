import 'package:flutter/material.dart';

class ActionButton extends StatelessWidget {
  const ActionButton({
    Key? key,
    this.margin = EdgeInsets.zero,
    this.title,
    this.child,
    this.boxShadows = const [],
    this.padding = EdgeInsets.zero,
    this.color,
    this.gradient,
    this.alignment,
    required this.onTap,
    this.border,
    this.textStyle,
    this.borderRadius = 20,
    this.shape = BoxShape.rectangle,
  })  : assert(color != null && gradient == null ||
            color == null && gradient != null),
        assert(title != null || child != null),
        super(key: key);

  final EdgeInsetsGeometry margin;
  final String? title;
  final Widget? child;
  final EdgeInsetsGeometry padding;
  final List<BoxShadow> boxShadows;
  final VoidCallback onTap;
  final Color? color;
  final Gradient? gradient;
  final AlignmentGeometry? alignment;
  final Border? border;
  final TextStyle? textStyle;
  final double borderRadius;
  final BoxShape shape;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(borderRadius),
        shape: shape,
        color: color,
        gradient: gradient,
        boxShadow: boxShadows,
        border: border,
      ),
      child: Material(
        type: MaterialType.transparency,
        child: InkWell(
          borderRadius: BorderRadius.circular(20),
          child: Align(
            alignment: alignment ?? Alignment.center,
            child: Padding(
              padding: padding,
              child: child ??
                  Text(
                    title!,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: textStyle ??
                        const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                  ),
            ),
          ),
          onTap: () {
            onTap();
          },
        ),
      ),
    );
  }
}
