import 'package:lingumoro/global/utils/my_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MyTextField extends StatefulWidget {
  const MyTextField({
    Key? key,
    this.margin = EdgeInsets.zero,
    this.controller,
    this.focusNode,
    this.maxLines,
    this.maxLength,
    this.maxLengthEnforcement,
    this.style,
    this.textInputAction,
    this.keyboardType,
    this.inputFormatters,
    this.hintText,
    this.hintStyle,
    this.textDirection,
    this.errorBorder,
    this.focusedBorder,
    this.enabledBorder,
    this.border,
    this.prefixIcon,
    this.suffix,
    this.suffixText,
    this.suffixIcon,
    this.onChanged,
    this.onSubmitted,
    this.validator,
    this.fill,
    this.fillColor,
    this.isPassword = false,
    this.boxShadow,
    this.borderRadius,
    this.isDense = false,
  }) : super(key: key);

  final EdgeInsetsGeometry margin;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final int? maxLines;
  final int? maxLength;
  final MaxLengthEnforcement? maxLengthEnforcement;
  final TextStyle? style;
  final TextInputAction? textInputAction;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final String? hintText;
  final TextStyle? hintStyle;
  final TextDirection? textDirection;
  final InputBorder? errorBorder;
  final InputBorder? focusedBorder;
  final InputBorder? enabledBorder;
  final InputBorder? border;
  final Widget? prefixIcon;
  final Widget? suffix;
  final String? suffixText;
  final Widget? suffixIcon;
  final ValueChanged<String>? onChanged;
  final ValueChanged<String>? onSubmitted;
  final FormFieldValidator<String>? validator;
  final bool? fill;
  final Color? fillColor;
  final List<BoxShadow>? boxShadow;
  final BorderRadiusGeometry? borderRadius;
  final bool isPassword;
  final bool isDense;

  @override
  State<MyTextField> createState() => _MyTextFieldState();
}

class _MyTextFieldState extends State<MyTextField> {
  bool showEye = true;
  bool passwordIsEncrypted = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.margin,
      decoration: BoxDecoration(
        borderRadius: widget.borderRadius,
        boxShadow: widget.boxShadow,
      ),
      child: widget.validator != null
          ? TextFormField(
              controller: widget.controller,
              focusNode: widget.focusNode,
              maxLines: widget.isPassword ? 1 : widget.maxLines,
              maxLength: widget.maxLength,
              maxLengthEnforcement: widget.maxLengthEnforcement,
              style: widget.style,
              textInputAction: widget.textInputAction,
              keyboardType: widget.keyboardType,
              inputFormatters: widget.inputFormatters,
              textDirection: widget.textDirection,
              decoration: InputDecoration(
                isDense: widget.isDense,
                hintText: widget.hintText,
                hintStyle: widget.hintStyle,
                filled: widget.fill,
                fillColor: widget.fillColor,
                prefixIcon: widget.prefixIcon,
                suffix: widget.suffix,
                suffixText: widget.suffixText,
                suffixIcon: widget.isPassword
                    ? GestureDetector(
                        child: passwordIsEncrypted
                            ? Container(
                                width: 25,
                                height: 25,
                                alignment: Alignment.center,
                                child:
                                    const Icon(Icons.remove_red_eye, size: 25),
                              )
                            : Container(
                                width: 25,
                                height: 25,
                                alignment: Alignment.center,
                                child: const Icon(MyIcons.eye_slash, size: 17),
                              ),
                        onTap: () {
                          setState(() {
                            passwordIsEncrypted = !passwordIsEncrypted;
                          });
                        },
                      )
                    : widget.suffixIcon,
                border: widget.border,
                enabledBorder: widget.enabledBorder,
                focusedBorder: widget.focusedBorder,
                errorBorder: widget.errorBorder,
              ),
              //ignore: avoid_bool_literals_in_conditional_expressions
              obscureText: widget.isPassword ? passwordIsEncrypted : false,
              onChanged: widget.onChanged,
              validator: widget.validator,
            )
          : TextField(
              controller: widget.controller,
              focusNode: widget.focusNode,
              maxLines: widget.isPassword ? 1 : widget.maxLines,
              maxLength: widget.maxLength,
              maxLengthEnforcement: widget.maxLengthEnforcement,
              style: widget.style,
              textInputAction: widget.textInputAction,
              keyboardType: widget.keyboardType,
              inputFormatters: widget.inputFormatters,
              textDirection: widget.textDirection,

              decoration: InputDecoration(
                isDense: widget.isDense,
                hintText: widget.hintText,
                hintStyle: widget.hintStyle,
                filled: widget.fill,
                fillColor: widget.fillColor,
                prefixIcon: widget.prefixIcon,
                suffix: widget.suffix,
                suffixText: widget.suffixText,
                suffixIcon: widget.isPassword
                    ? GestureDetector(
                        child: passwordIsEncrypted
                            ? Container(
                                width: 25,
                                height: 25,
                                alignment: Alignment.center,
                                child: const Icon(Icons.remove_red_eye),
                              )
                            : Container(
                                width: 25,
                                height: 25,
                                alignment: Alignment.center,
                                child: const Icon(MyIcons.eye_slash, size: 17),
                              ),
                        onTap: () {
                          setState(() {
                            passwordIsEncrypted = !passwordIsEncrypted;
                          });
                        },
                      )
                    : widget.suffixIcon,
                border: widget.border,
                enabledBorder: widget.enabledBorder,
                focusedBorder: widget.focusedBorder,
                errorBorder: widget.errorBorder,
              ),
              //ignore: avoid_bool_literals_in_conditional_expressions
              obscureText: widget.isPassword ? passwordIsEncrypted : false,
              onChanged: widget.onChanged,
              onSubmitted: widget.onSubmitted,
            ),
    );
  }
}
