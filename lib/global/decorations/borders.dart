import 'package:flutter/material.dart';

late InputBorder border;
late InputBorder focusedBorder;
late InputBorder errorBorder;

void buildBorders() {
  border = OutlineInputBorder(
    borderRadius: BorderRadius.circular(20),
    borderSide: BorderSide(
      width: 2,
      color: Colors.grey.shade500,
    ),
  );

  focusedBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(20),
    borderSide: BorderSide(
      width: 3.5,
      color: Colors.grey.shade800,
    ),
  );

  errorBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(20),
    borderSide: BorderSide(
      width: 3.5,
      color: Colors.red.shade700,
    ),
  );
}
